name := "dbq"

version := "1.0"

scalaVersion := "2.10.6"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.5.1",
  "org.apache.spark" %% "spark-sql" % "1.5.1",
  "org.postgresql" % "postgresql" % "9.4-1205-jdbc42"
)

seq(flywaySettings: _*)

// "createdb profiles" to create this
flywayUrl := "jdbc:postgresql:profiles"

flywayUser := System.getenv("USER")
