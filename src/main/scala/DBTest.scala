import java.sql.{Connection, DriverManager}
import org.apache.spark.rdd.RDD
import org.apache.spark.{sql, SparkContext, SparkConf}
import org.apache.spark.sql.{Row, DataFrame}

object DBTest {
  def getEmailFromId(id: BigInt, df: DataFrame) : String = {
//    val filterString = s"id = $id"
    val filterString = "id < 30"
//    val row = df.filter(filterString)
    val row = df
    println(s"gefi: $filterString")
//    println(row)
    println(row.columns.toString)
//    val rowContents = row.collect()
    val rowContents = df.collect()
    println("gefirc")
    println(rowContents)
    println("gefix")
    println("gefipairs")
    val pairs = row.rdd.map { case Row(id: BigInt, email: String) => id -> email }
//    println(pairs)
    pairs.map(println)
//    val pairs = df("SELECT id, email FROM emails").rdd.map { case Row(key: Int, value: String) => key -> value }
    "foo"
  }

  def main(args: Array[String]): Unit = {
    val dbURL = "jdbc:postgresql://localhost/profiles"
    val profileTableName = "profiles"
    print("start")

    val config = new SparkConf()
      .setAppName("dbtest")
      .set("spark.master", "local")
    val sparkContext = new SparkContext(config)
    print("class")
    Class.forName("org.postgresql.Driver")
    println("sc")
    val dbc: Connection = DriverManager.getConnection(dbURL)
    println("dbc")
    val sqlContext = new sql.SQLContext(sparkContext)
    println("sql")

    import sqlContext.implicits._
    case class Email(id: BigInt, email: String)

    //    val query_table = "(SELECT email, CAST(profile_id AS TEXT) FROM schema.profiles) AS casted_table"
    val jdbc = sqlContext.read.format("jdbc").options(Map("url" -> dbURL, "dbtable" -> "emails")).load()
//    val jdbc = sqlContext.load("jdbc", Map("url" -> dbURL, "dbtable" -> "profiles"))
    print("jdbc")
//    print(jdbc)
    jdbc.select("id", "email")
//    jdbc.select("email").show()
    jdbc.show()

    println("lookup")
    getEmailFromId(2, jdbc)
    println("jdbc map")
//    val jm = jdbc.map(e => (e(0).asInstanceOf[BigInt] -> e(1).asInstanceOf[String]))
//val jm = jdbc.collect().toMap[BigInt, String]
    val jm = jdbc.collectAsList()
    println("jdbc map print")
    println(jm)
    //jdbc.map(println)
//    jdbc.collect().foreach(println)
//    val foo: RDD[Email] = jdbc.select("id", "email")


//    jdbc.map(x => (x.email, x.profile_id.toArray))
//    jdbcDF.map(x => (x.id, x.values.toArray))

    println("dlfsfsdfsf")
    /*
    */
    //  val table = sqlContext.table(profileTableName)
    //  val jdbcDF = sqlContext.read.format("jdbc").options(
    //    Map("url" -> dbURL,
    //      "dbtable" -> "schema.tablename")).load()

    sparkContext.stop()
  }
}
