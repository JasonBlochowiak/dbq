CREATE TABLE emails(
	id SERIAL NOT NULL,
    email text NOT NULL,
    PRIMARY KEY(id)
);
CREATE UNIQUE INDEX emails_id_key ON emails (id);
CREATE UNIQUE INDEX emails_email_key ON emails (email);

CREATE TABLE profiles(
	profile_id text NOT NULL,
	email_id bigint NOT NULL,
	CONSTRAINT email_id_fkey FOREIGN KEY (email_id)
	      REFERENCES emails (id) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE CASCADE
);
CREATE UNIQUE INDEX profiles_key ON profiles (profile_id);
